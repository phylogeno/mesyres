#!/usr/bin/env perl

use Modern::Perl '2011';
use warnings;
use Smart::Comments '###';
use autodie;
use List::Util qw(all);
use List::Util qw( min max );
use Getopt::Euclid qw(:vars);
use Sort::Naturally;
use List::MoreUtils qw(first_index);
use Tie::IxHash;


my %gbk_of;
my %id_of;

for my $infile (@ARGV_infiles) {
    #gbk part
    my $locusiter = 0;
    my $locus;
    my $gca;
    #gca maybe absent from gbk informations
    $gca = $infile;
    $gca =~ s/\.gb//;
    $gca =~ s/\S+\///;
    my $orgiter = 0;
    my $org;
    my $featiter = 0;
    my $genepart = "sleep";
    #gene part
    my $genenumber;
    #iter
    my $geneiter = 0;
    my $protiter = 0;
    my $strand = 0;
    my $coo = 0;
    my $product = "NA";
    my $id = 0;
    my $gi = 0;
    my $translation = 0;
    my $translationend = 0;
    my $seq = undef;
    #open outfile if create fasta
    my $outfile;
    my $out;
    if ($ARGV{'--create-fasta'} eq "yes") {
        $outfile = $infile;
        $outfile =~ s/\.gb//;
        $outfile =~ s/\S+\///;
        $outfile = "$outfile-abbr.fasta";
        open $out, '>', $outfile;
    }
    #open gbk file
    open my $in, '<', $infile;
    while (my $line = <$in>) {
        chomp $line;
        #get locus of first scaffolds
        if ($line =~ /LOCUS/){
            if ($locusiter == 0) {
                $line =~ s/\s+/\t/g;
                my @chunks = split /\t/, $line;
                $locus = $chunks[1];
            }
            $locusiter++;
        }
        #get ncbi name
        if ($line =~ /ORGANISM/) {
            if ($orgiter == 0) {
                #intwo step because of space in name
                $line =~ s/\s+/\t/;
                my @chunks = split /\t/, $line;
                my $chunk = $chunks[1];
                $chunk =~ s/\s+/\t/;
                @chunks = split /\t/, $chunk;
                $org = $chunks[1];
                $org =~ s/\'//g;
            }
            $orgiter++
        }
        #begin gene part
        if ($line =~ /FEATURES/) {
            if ($featiter == 0) {
                $genepart = "activate";
            }
            $featiter++;
        }
        #get gene informations
        #get position and strand
        if ($genepart eq "activate") {
            if ($line =~ / \bCDS\b /) {
                #close current open genepart
                #close open gene part
                #close iter
                $geneiter = 0;
                $protiter = 0;
                $strand = 0;
                $coo = 0;
                $product = "NA";
                $id = 0;
                $gi = 0;
                $translation = 0;
                $translationend = 0;
                $seq = undef;
                #new gene part
                $geneiter++;
                if ($line =~ /\bcomplement\b/) {
                    $strand = -1;
                }
                else {
                    $strand = 1;
                }
                my $temp = $line;
                $temp =~ s/S+//;
                $temp =~ s/\s+/\t/g;
                my @chunk = split /\t/, $temp;
                $coo = $chunk[2];
                if ($coo) {
                    $coo =~ s/complement//g;
                    $coo =~ s/\(|\)//g;
                    $coo =~ s/\.\./-/g;
                }
            }
            #get product
            if ($line =~ "/product=") {
                $geneiter++;
                my @chunk = split /\=/, $line;
                $product = $chunk[1];
                $product =~ s/\"//g;
            }
            #get prot id
            if ($line =~ "/protein_id") {
                $geneiter++;
                $protiter++;
                my @chunk = split /\=/, $line;
                $id = $chunk[1];
                $id =~ s/\"//g;
            }
            #get GI
            if ($line =~ "/db_xref=\"GI") {
                $geneiter++;
                my @chunk = split /\:/, $line;
                $gi = $chunk[1];
                $gi =~ s/\"//g;
                #load in hash
                if ($geneiter >= 3) {
                    $genenumber++;
                    $geneiter = 4;
                    $id = $id . "-" . $genenumber . "-" . $locusiter;
                    $gbk_of{$gca}{"acc"} = $locus;
                    $gbk_of{$gca}{"org"} = $org;
                    $gbk_of{$gca}{$id}{"coo"} = $coo;
                    $gbk_of{$gca}{$id}{"strand"} = $strand;
                    #smt product miss, unknown in place
                    if ($product eq "NA") {
                        $gbk_of{$gca}{$id}{"product"} = "unknown";
                    }
                    else {
                        $gbk_of{$gca}{$id}{"product"} = $product;
                    }
                    $gbk_of{$gca}{$id}{"gi"} = $gi;
                    $gbk_of{$gca}{$genenumber} = $id;
                    $gbk_of{$gca}{"max"} = $genenumber;
                }
            }
            #create fasta file (abbr format) if activate
            if ($geneiter == 4) {
                if ($ARGV{'--create-fasta'} eq "yes") {
                    #get translation first part
                    if ($line =~ "/translation=") {
                        $translationend = 0;
                        $translation++;
                    }
                    #get other part of sequence
                    if (($translation == 1) && ($translationend <= 1)) {
                        my $part = $line;
                        $part =~ s/ //g;
                        $part =~ s/\/translation\=//g;
                        $part =~ s/\"//g;
                        $seq = $seq . $part;
                    }
                    #count end of sequence by " sometimes two " in a line (short sequence).
                    my @counts = $line =~ /\"/g;
                    for my $count (@counts) {
                        $translationend++;
                    }
                    if (($translation == 1) && ($translationend == 2)) {
                        #write fasta (60 characters per lines)
                        print{$out} ">" . $gca . "|" . $id . "\n";
                        my $len = length $seq;
                        if ($len <= 60) {
                            print{$out} $seq . "\n";
                        }
                        else {
                            for (my $i = 0; $i <= $len; $i += 60) {
                                my $seg =  substr($seq, $i, 60);
                                print {$out} $seg . "\n";
                            }
                        }
                        $translationend++;
                    }
                }
            }
        }
    }
}
### %gbk_of

if ($ARGV{'--create-defline'} eq "yes") {
    my $dir = $ARGV{'--ogpath'};
    my @files = glob( $dir . '/*' );

    #rename fasta
    for my $file (@files) {
        #open outfile
        my $rename = $file;
        $rename =~ s/\.fasta//g;
        $rename = "$rename-st.fasta";
        open my $ren, '>', $rename;
        #open infile
        open my $in, '<', $file;
        while (my $line = <$in>) {
            chomp $line;
            #replace defline
            if ($line =~ ">") {
                my @deflines = split /\|/, $line;
                my $gca = $deflines[0];
                $gca =~ s/\>//;
                my $max = $gbk_of{$gca}{"max"};
                my $id = $deflines[1];
                my @ids = split /\-/, $id;
                my $genenumber = $ids[1];
                my $genenumberadd = $genenumber + 1;
                my $genenumbermin = $genenumber - 1;
                #place in fonction of regex main script
                my $org = $gbk_of{$gca}{"org"};
                my $acc = $gbk_of{$gca}{"acc"};
                my $product = $gbk_of{$gca}{$id}{"product"};
                my $coo = $gbk_of{$gca}{$id}{"coo"};
                my $strand = $gbk_of{$gca}{$id}{"strand"};
                if ($genenumbermin == 0) {
                    my $nextid = $gbk_of{$gca}{$genenumberadd};
                    print{$ren} ">GI:$id|strand:$strand|assembly:$gca|org:$org|acc:$acc|product:$product|coo:$coo|neighbours:GI:$nextid(1),start" . "\n";
                }
                elsif ($genenumber == $max) {
                    my $previousid = $gbk_of{$gca}{$genenumbermin};
                    print{$ren} ">GI:$id|strand:$strand|assembly:$gca|org:$org|acc:$acc|product:$product|coo:$coo|neighbours:end,GI:$previousid(-1)" . "\n";
                }
                else {
                    my $nextid = $gbk_of{$gca}{$genenumberadd};
                    my $previousid = $gbk_of{$gca}{$genenumbermin};
                    print{$ren} ">GI:$id|strand:$strand|assembly:$gca|org:$org|acc:$acc|product:$product|coo:$coo|neighbours:GI:$nextid(1),GI:$previousid(-1)" . "\n";
                }
            }
            else {
                print{$ren} $line . "\n";
            }
        }
    }
}



__END__

=head1 NAME

gbk-reader.pl - Create FASTA files from NCBI GBK files with unique protein ids

=head1 VERSION

version 0.1

=head1 USAGE

    gbk-reader.pl <infiles> [optional arguments]

=head1 REQUIRED ARGUMENTS

=over

=item <infiles>

Path to input GBK files in format GCA/GCA.gb [repeatable argument].

=for Euclid:
    infiles.type: readable
    repeatable

=back

=head1 OPTIONS

=over

=item --create-fasta=<bool>

Create fasta file from GBK if yes [default: bool.default].

=for Euclid:
    bool.type: string
    bool.default: 'no'

=item --create-defline=<bool>

Create new deflines from OrthoFinder output if yes [default: bool.default].

=for Euclid:
    bool.type: string
    bool.default: 'no'

=item --ogpath=<dir>

Path to OG FASTA directory (incl. only FASTA files) [default: dir.default].

=for Euclid:
    dir.type: readable
    dir.default: 'fasta'

=back

=head1 AUTHOR

Luc CORNET <luc.cornet@uliege.be>

=head1 CONTRIBUTOR

Denis BAURAIN <denis.baurain@uliege.be>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Liege / Unit of Eukaryotic
Phylogenomics / Luc CORNET and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
