#!/usr/bin/env perl

use Modern::Perl '2011';
use warnings;
use Smart::Comments '#####';
use autodie;
use List::Util qw(all);
use List::Util qw( min max );
use Getopt::Euclid qw(:vars);
use Path::Class qw(dir file);
use Sort::Naturally;
use List::MoreUtils qw(first_index);
use Tie::IxHash;
use File::Find::Rule;
use Storable qw(lock_store lock_nstore lock_retrieve);

use Config::Any;
use Path::Class qw(dir file);
use Bio::MUST::Tools;
use aliased 'Bio::MUST::Tools::Debrief42';
use aliased 'Bio::MUST::Core::Ali';

### Load list
#Load negate list of assembly(ies)
my %negate_of;
unless ($ARGV{'--forty2'}) {
    %negate_of = load_negate($ARGV{'--negate-path'}) if ($ARGV{'--negate-path'} ne "no");
}

### Load OGs
#load directory with OGs in format post gbk-reader
my @files;
unless ($ARGV{'--forty2'}) {
    @files = File::Find::Rule
        ->file()
        ->name( qr{\-st.fasta \z}xmsi )
        ->in($ARGV{'<indir>'})
    ;
}

# load 42 objects part
my $run_report;
if ($ARGV{'--forty2'}) {
#if (1 == 2) {
    # Load FortyTwo's YAML config file
    my $config_file = file('forty-good', 'sca-4-ana.yaml');
    my $config = Config::Any->load_files( {
        files           => [ $config_file ],
        flatten_to_hash => 1,
        use_ext         => 1,
        }
    );
    # Sepicfy tax-reports directory
    my $reports_dir = dir('forty-good', 'tes')->stringify;
    # Build Debrief42 object
    my $deb42 = Debrief42->new(
        config      => $config->{$config_file},
        reports_dir => $reports_dir,
        );
    # Build RunReport object
    $run_report = $deb42->run_report;
}

### Filtering OGs
my %keep_infiles;
unless ($ARGV{'--forty2'}) {
    %keep_infiles = filtering_OGs($ARGV{'--pairwise-similarity'}, \@files) if ($ARGV{'--filtering'});
}
##### %keep_infiles

### Parse OGs
# Parse orthologous groups (take hashes reference)
my ($accesion_of, $seen_gi, $central_gi, $basic_OG, $strand_for, $function_record, $assembly_id, $assembly_acc, $origin_mcl);
($accesion_of, $seen_gi, $central_gi, $basic_OG, $strand_for,
        $function_record, $assembly_id, $assembly_acc, $origin_mcl) = parsing_filtered( \%keep_infiles ) unless $ARGV{'--forty2'};
#parse 42 adds
my %scaffold_from;
my %id_of;
my %org_of;
my %meta_org;
my %outfile_from;

if ($ARGV{'--forty2'}) {
    my $ref = lock_retrieve('mesyreslog2');
    $assembly_acc = $ref->{assembly_acc};
    $strand_for = $ref->{strand_for};
    my %strand_for = %{$strand_for};
    $seen_gi = $ref->{seen_gi};
    my %seen_gi = %{$seen_gi};
    $function_record = $ref->{function_record};
    $assembly_id = $ref->{assembly_id};
    my %assembly_id = %{$assembly_id};
    my %coo_of;
    my %info_of;
    my $uniq;
    #process according to org
    for my $org ( $run_report->all_orgs ) {
        #define a false accesion for metagnome
        $uniq++;
        my $Facc = 'NO' . $uniq;
        $meta_org{$Facc} = $org;
        # Process according to outfiles
        my $org_report = $run_report->org_report_for($org);
        for my $outfile ($org_report->all_outfiles) {
            my $new_seqs_by_outfile = $org_report->new_seqs_by_outfile_for($outfile);
            my $ali = Ali->load($outfile);
            # Process according to new_seq
            NEW:
            for my $new_seq ( @$new_seqs_by_outfile ) {
                my $seq_id = $new_seq->seq_id;
                my $acc = $new_seq->acc;
                my $min = $new_seq->start;
                my $max = $new_seq->end;
                my $strand = $new_seq->strand;
                my @seqs = $ali->filter_seqs( sub { index ($_->full_id, $seq_id) >= 0 } );
                my $seq = shift @seqs;
                next NEW unless $seq;
                my $degap_seq = $seq->raw_seq;
                $outfile =~ s/forty\-good\/tes\///;
                $outfile =~ s/\-col\S+//;
                my $start = $new_seq->start;
                my $strand = $new_seq->strand;
                my $id = $seq_id . '-' . $outfile;
                $coo_of{$seq_id}{$id}= $start;
                $org_of{$id} = $Facc;
                $scaffold_from{$outfile}{$seq_id}++;
                $outfile_from{$outfile}{$id}++;
                $info_of{$id}{'min'} = $min;
                $info_of{$id}{'max'} = $max;
                $info_of{$id}{'strand'} = $strand;
                $info_of{$id}{'seq'} = $degap_seq if $seq;
                $info_of{$id}{'seq'} = 'NA' unless $seq;
            }
        }
    }
    #create on order for new seq in scaffold
    my @scaffolds = keys %coo_of;
    for my $scaffold (@scaffolds) {
        my $order;
        my %coo_from = %{$coo_of{$scaffold}};
        my @coos = sort { $coo_from{$a} <=> $coo_from{$b}} keys %coo_from;
        for my $coo (@coos) {
            $order++;
            my $min = $info_of{$coo}{'min'};
            my $max = $info_of{$coo}{'max'};
            my $strand = $info_of{$coo}{'strand'};
            my $seq = $info_of{$coo}{'seq'};

            my @chunks = split /\-/, $coo;
            my $finalid = $chunks[0];
            $finalid =~ s/\-/\_/;
            my $Facc = $org_of{$coo};
            my $Fid = $finalid . '-' . $order . '/' . $Facc;
            $id_of{$coo} = $finalid . '-' . $order . '/' . $Facc;
            $strand_for{$Fid} = $strand;
            $seen_gi{$Fid}{'min'} = $min;
            $seen_gi{$Fid}{'max'} = $max;
            $seen_gi{$Fid}{'seq'} = $seq;
            my $org = $meta_org{$Facc};
            $assembly_id{$Fid} = $org;
        }
    }
    $seen_gi = \%seen_gi;
    $strand_for = \%strand_for;
    $assembly_id = \%assembly_id;
}
my $metac = keys %meta_org;

### Construction of triplet
my %triplet_gi = trio_neighbours($central_gi, $seen_gi) unless $ARGV{'--forty2'};

### Reconstruction of neighbour stretch
my ($neighbour_group, $neighbour_groupcopy1, $inverse_group, $neighbour_access) = stretch_neighbour( \%triplet_gi,
    $central_gi, $seen_gi ) unless $ARGV{'--forty2'};

### Making colinear group
my ($neighbour_group1, $colinear_of, $neighbour_gi, $accesions );
($neighbour_group1, $colinear_of, $neighbour_gi, $accesions ) = make_colinear( $neighbour_group, $accesion_of,
    $neighbour_access ) unless $ARGV{'--forty2'};


#define scalar for count the number OGs syntenic
my $syngen = 0;
#to sort all syntenic block
my %syntenic_block;
my %event_record;
my %OG_ofcopy;
my %basic_OG;
#to print fasta file
my %inversion_of;

#open outputs files for map
my $coomap = $ARGV{'--coordinate-map'};
open my $cooout, '>', $coomap;
my $gbkmap = $ARGV{'--gbknumber-map'};
open my $gbkout, '>', $gbkmap;

### Processing colinear group
my %load_of;
tie my %colinear_of, 'Tie::IxHash';

unless ($ARGV{'--forty2'}) {
    %colinear_of = %{$colinear_of};
    lock_store { colinear_of =>  \%colinear_of, }, 'mesyreslog1';
}
if ($ARGV{'--forty2'}) {
    my $ref = lock_retrieve('mesyreslog1');
    my $colinear_of = $ref->{colinear_of};
    %colinear_of = %{$colinear_of};
    my $ref = lock_retrieve('mesyreslog2');
    my $OG_ofcopy = $ref->{OG_ofcopy};
    %OG_ofcopy = %{$OG_ofcopy};
    $basic_OG = $ref->{basic_OG};
    %basic_OG = %{$basic_OG};
}

my @colinears = sort keys %colinear_of;
my %OGlist_of;
COLINEAR:
for my $colinear (@colinears) {
    #Format colinear group in hashes
    my ($OG_of1, $OG_ofcopy, $fromcolinears, $OGlist_of) = format_colinear($colinear, $colinear_of, \%OG_ofcopy, $seen_gi,
        $neighbour_group, \%OGlist_of) unless $ARGV{'--forty2'};
    %OGlist_of = %{$OGlist_of} unless $ARGV{'--forty2'};

    unless ($ARGV{'--forty2'}) {
        %OG_ofcopy = %{$OG_ofcopy};
        lock_store { OG_ofcopy =>  \%OG_ofcopy, assembly_acc => $assembly_acc, strand_for => $strand_for,
            seen_gi => $seen_gi, function_record => $function_record, assembly_id => $assembly_id,
            basic_OG => $basic_OG }, 'mesyreslog2';
    }
    if ($ARGV{'--forty2'}) {
        #my $ref = lock_retrieve('mesyreslog2');
    }

    #Chech if colinear group has enough genome to be considered
    unless ($ARGV{'--forty2'}) {
        my $statut = colinear_presence($accesions, $OG_of1, $ARGV{'--mingenome-colinear'}, $accesion_of);
        next COLINEAR if $statut eq 'next';
    }

    #construct block in iterating ordering and opposite orientation
    my $iteration = 0;
    tie my %orderiter_of, 'Tie::IxHash';
    my ($accesnumber, $order_of);
    my @orderedOGs;

    #if prerun or run of 42.
    $ARGV{'--iteration-cycle'} = 0 if $ARGV{'--forty2'};

    while ($iteration <= $ARGV{'--iteration-cycle'}) {
        #Colinear group ordering by iteration
        my ($orderedOGs, $orderacces, $OG_of, $neighbour_group, $neighbour_groupcopy);
        ($orderedOGs, $orderacces, $OG_of, $neighbour_group, $neighbour_groupcopy) = colinear_ordering($colinear,
            $fromcolinears, $neighbour_groupcopy1, $neighbour_group1, $OG_of1, $seen_gi, $iteration) unless $ARGV{'--forty2'};
        #For sorting by reversal
        my $reverse = 0;
        my %comparing_block;
        my %rcomparing_block;
        my %meta_acc;

        #make ordered acces array
        ($accesnumber, $order_of) = make_orderacces($accesion_of, $orderacces) unless $ARGV{'--forty2'};

        unless ($ARGV{'--forty2'}) {
            $load_of{$colinear}{accesnumber} = $accesnumber;
            $load_of{$colinear}{order_of} = $order_of;
            $load_of{$colinear}{orderedOGs} = $orderedOGs;
            $load_of{$colinear}{OG_of} = $OG_of;
            $load_of{$colinear}{neighbour_gi} = $neighbour_gi;
            lock_store { load_of =>  \%load_of, }, 'mesyreslog3';
        }
        if ($ARGV{'--forty2'}) {
            #reload non metagenomic part
            my $ref = lock_retrieve('mesyreslog3');
            my $load_of = $ref->{load_of};
            %load_of = %{$load_of};
            $accesnumber = $load_of{$colinear}{accesnumber};
            $order_of = $load_of{$colinear}{order_of};
            $orderedOGs = $load_of{$colinear}{orderedOGs};
            $OG_of = $load_of{$colinear}{OG_of};
            $neighbour_gi = $load_of{$colinear}{neighbour_gi};
                #load metagenome part
                #take all scaffold of a colinear group
                my @orderedOGs1 = @{$orderedOGs};
                my %scaffold_colinear;
                for my $OG (@orderedOGs1) {
                    $OG =~ s/\.\.\/404\///;
                    $OG =~ s/forty\/ali\///;
                    $OG =~ s/\-st\.fasta//;
                    $OG =~ s/Orthogroups\///;
                    my @scaffolds;
                    if ($scaffold_from{$OG}) {
                        my %scaffold_ref = %{$scaffold_from{$OG}};
                        @scaffolds = keys %scaffold_ref;
                    }
                    for my $scaffold (@scaffolds) {
                        $scaffold_colinear{$scaffold}++;
                    }
                }
                #make presence stat on group
                my @scaffolds =  keys %scaffold_colinear;
                my $size = @orderedOGs1;
                for my $scaffold (@scaffolds) {
                    my $presence;
                    for my $OG (@orderedOGs1) {
                        $presence++ if $scaffold_from{$OG}{$scaffold};
                    }
                    my $presencestat = ($presence/$size) * 100;
                    #delete scaffold based on a threshold
                    if ($presencestat <= 0) {
                        delete $scaffold_colinear{$scaffold};
                        #next COLINEAR;
                    }
                }
                #give a uniq accesion to scaffold
                @scaffolds =  keys %scaffold_colinear;
                my $acccount;
                for my $scaffold (@scaffolds) {
                    $acccount++;
                    $scaffold_colinear{$scaffold} = 'ACC' . $acccount;
                    #asses presence multiple time of same scaffold
                    for my $OG (@orderedOGs1) {
                        say 'Warning mpore than one time scaffold : ' . $scaffold . ' in OG : ' . $OG
                            if $scaffold_from{$OG}{$scaffold} > 1;
                    }
                }
                #add metagenome information in previous structure
                #dref
                my @laccesnumber = @{$accesnumber};
                my %lneighbour_gi = %{$neighbour_gi};

                my $ref = lock_retrieve('mesyreslog2');
                $assembly_acc = $ref->{assembly_acc};
                my %assembly_acc = %{$assembly_acc};
                my $count;
                OG:
                for my $OG (@orderedOGs1) {
                    next OG unless $outfile_from{$OG};
                    my %seqs_of = %{$outfile_from{$OG}};
                    my @seqs = keys %seqs_of;
                    SEQ:
                    for my $new_seq ( @seqs ) {
                        my $id = $new_seq;
                        my $acc = $id;
                        $acc =~ s/\-OG\S+//;
                        next SEQ unless $scaffold_colinear{$acc};
                        my $prefix = $scaffold_colinear{$acc};
                        my $finalid = $id_of{$id};
                        my $cid = $prefix . "_" . $finalid;
                        my $cOG = 'Orthogroups/' . $OG . '-st.fasta';
                        #add
                        $OG_ofcopy{$cOG}{$cid}++;
                        $lneighbour_gi{$cid}++;
                        $basic_OG{$cOG}{$cid}++;
                        my %include_of = map { $_ => 1 } @laccesnumber;
                        unless ($include_of{$prefix}) {
                            $count++;
                            push @laccesnumber, $prefix;
                            $meta_acc{$prefix}++;
                            $assembly_acc{$prefix} = $acc;
                        }
                    }
                }
                #filter group, min 50 % of metagenomes
                my $lim = $metac/2;
                #next COLINEAR if $count < $lim;
                #make new ref
                $neighbour_gi = \%lneighbour_gi;
                $accesnumber = \@laccesnumber;
                $assembly_acc = \%assembly_acc;

            #}

        }

        #Do the creation of block and sorting by reversal in two diffrent orientation
        while ($reverse <= 1) {
            #Sorting by reversal (in orientation define by $reverse)
            my ($absence_of, $event_of, $revert_of, $unfound_of, $end_record, $limit_of, $OG_ofcopy, $orderedOGs ) =
                sorting_byreversal($accesnumber, $basic_OG, $neighbour_gi, \%OG_ofcopy, $orderedOGs, $reverse, \%meta_acc);

            # Syntenic block creation
            my $blockkey;
            my ($comparing_block, $rcomparing_block, $event_record, $blockkey, $orderedOGs) = block_creation($orderedOGs,
                $absence_of, $event_of, $revert_of, $unfound_of, $end_record, $limit_of,
                $blockkey, $ARGV{'--maxgenome-block'}, $ARGV{'--inversion'}, $ARGV{'--minsize-block'},
                \%comparing_block, \%rcomparing_block, $colinear, $reverse);

            #get hashes out of while loop
            %comparing_block = %{$comparing_block};
            %rcomparing_block = %{$rcomparing_block};
            %event_record = %{$event_record};
            $blockkey = $blockkey;
            @orderedOGs = @{$orderedOGs};
            #activate reverse tag
            $reverse++;
        }

        #compare the two orientations and chose the one with biggest group
        my ($syntenic_block, $event_record, $syngene, $orientation) = compare_orientation(\%comparing_block, \%rcomparing_block,
            \%event_record, \%syntenic_block, $syngen);
        #load iteration
        $orderiter_of{'syngen'}{$iteration} = $syngene;
        $orderiter_of{'iter'}{$iteration} = $iteration;
        $orderiter_of{'synblock'}{$iteration} = $syntenic_block;
        $orderiter_of{'event'}{$iteration} = $event_record;
        $orderiter_of{'orientation'}{$iteration} = $orientation;
        #loop complete
        $iteration++;
    }

    #compare iteration in ordering
    my ($syntenic_block, $event_record, $syngene, $orientation) = compare_iter(\%orderiter_of);
    #register block and event
    %syntenic_block = %{$syntenic_block};
    %event_record = %{$event_record};
    #load incremental number of syngene outof colinear loop $syngen
    $syngen = $syngene;
    #print map
    print_map($colinear, $accesnumber, $assembly_acc, $order_of, \%OG_ofcopy, $strand_for,
        $seen_gi, $cooout, $gbkout, $orientation, \@orderedOGs);
}
say "number of syntenic OGs" . "=" . "\t" . $syngen;

### Print syntenic block

#open summary outfile
my $summary = $ARGV{'--summary'};
open my $sum, '>', $summary;

#print OG implicate in colinear group
unless ($ARGV{'--forty2'}) {
    lock_store { OGlist_of =>  \%OGlist_of, }, 'mesyreslog4';
}
if ($ARGV{'--forty2'}) {
    my $ref = lock_retrieve('mesyreslog4');
    my $OGlist_of = $ref->{OGlist_of};
    %OGlist_of = %{$OGlist_of};
}
print_OG (\%OGlist_of, \%OG_ofcopy, $assembly_id, $seen_gi) if ($ARGV{'--print-col'});;

#Sort syntenic block on size for print in order
foreach my $id ( sort { scalar(@{$syntenic_block{$b}}) <=> scalar(@{$syntenic_block{$a}}) } keys %syntenic_block ) {

    #print summary file
    print_sum (\%syntenic_block, $id, \%event_record, \%OG_ofcopy, $function_record);

    #print fasta file
    print_fasta (\%syntenic_block, \%OG_ofcopy, $id, $assembly_id, $seen_gi) if ($ARGV{'--print-fasta'});

}


##function defining

#extract list of undesire assembly(ies)
sub load_negate {
    my $negate = shift;

    my %negate_of;
    open my $neg, '<', $negate;
    while (my $line = <$neg>) {
        chomp $line;
        #for multi column file
        my @chunks = split /\t/, $line;
        my $gca = $chunks[0];
        $negate_of{$gca}++;
    }

    return %negate_of;
}

#filtering of OGs based on neighbour informations
sub filtering_OGs {
    my $similarity = $_[0];
    my @files = @{$_[1]};

    my %genome_number;
    my %file_acc;
    my %keep_of;
    #calcul the number of genomes
    for my $infile (@files) {
        open my $in, '<', $infile;
        L1:
        while (my $line = <$in>) {
            chomp $line;
            if ($line =~ /(GI:)/){
                my $assembly = get_assembly($line);
                next L1 if $negate_of{$assembly};
                my $acces = get_acces($line);
                $genome_number{$acces}++;
                $file_acc{$infile}{$acces}++;
                $keep_of{$infile}++;
            }
        }
    }
    my $genomenumber = keys %genome_number;
    #treshold on percent of genome in OGs, put in %keep_infiles
    my $genometreshold = $similarity;
    #ignore empty file (after negate list)
    my @keeps = sort keys %keep_of;
    for my $infile (@keeps) {
        my %acc_in = %{$file_acc{$infile}};
        my $accin = keys %acc_in;
        my $calcul = (($accin / $genomenumber) * 100);
        $keep_infiles{$infile}++ if $calcul >= $genometreshold;
    }
    #### Filter presence genomes
    #delete from infile the OGs don't pass test pairwise
    my %syn_file;
    my @infiles = sort keys %keep_infiles;
    for my $infile (@infiles) {
        #stock neihgbour
        my %neihgbour_of;
        open my $in, '<', $infile;
        L2:
        while (my $line = <$in>) {
            chomp $line;
            if ($line =~ /(GI:)/){
                my $assembly = get_assembly($line);
                next L2 if $negate_of{$assembly};
                #exit the start and end of chromosome
                next if $line =~ / \bstart\b /xms;
                next if $line =~ / \bend\b /xms;
                #get neighours
                my ($preGI, $postGI) = get_neighbour($line);
                #make GI uniq by passing accesion after
                my $acces = get_acces($line);
                my $apreGI = $preGI . $acces;
                my $apostGI = $postGI .$acces;
                $neihgbour_of{$apreGI}++;
                $neihgbour_of{$apostGI}++;
            }
        }
        for my $inslave (@infiles) {
            my $neighcount;
            my %inslave_count;
            open my $in, '<', $inslave;
            L3:
            while (my $line = <$in>) {
                chomp $line;
                if ($line =~ /(GI:)/){
                    my $assembly = get_assembly($line);
                    next L3 if $negate_of{$assembly};
                    #take central gi
                    #get central GI of MCL group
                    my $GI = get_central($line);
                    #make GI uniq
                    my $acces = get_acces($line);
                    my $aGI = $GI . $acces;
                    $inslave_count{$aGI}++;
                    $neighcount++ if $neihgbour_of{$aGI};
                }
            }
            my $inslavecount = keys %inslave_count;
            my $pres;
            if ($neighcount) {
                $pres = (($neighcount/$inslavecount) * 100);
                $syn_file{$inslave}++ if $pres >= $similarity;
            }
        }
    }
    #replace keep file
    %keep_infiles = %syn_file;

    return %keep_infiles;
}

#Parse filtered OG to collect inforations on deflines
sub parsing_filtered {
    tie my %keep_infiles, 'Tie::IxHash';
    %keep_infiles = %{$_[0]};

    my @infiles = sort keys %keep_infiles;
    my %seen_gi;
    tie my %central_gi, 'Tie::IxHash';
    my %accesion_of;
    my %strand_for;
    my %origin_mcl;
    my %basic_OG;
    my %protein_order;
    my %function_record;
    my %assembly_id;
    my %assembly_acc;

    for my $infile (@infiles) {
        open my $in, '<', $infile;
        my $cGI;
        my $cSeq;
        LINE:
        while (my $line = <$in>) {
            chomp $line;
            if ($line =~ /(GI:)/){
                #stock previous sequence
                $seen_gi{$cGI}{seq}= $cSeq if $cGI;
                $cSeq = undef;
                my $assembly = get_assembly($line);
                next LINE if $negate_of{$assembly};
                #exit the start and end of chromosome
                next if $line =~ / \bstart\b /xms;
                next if $line =~ / \bend\b /xms;
                #get GI of MCL group
                my $GI = get_central($line);
                #get neighours
                my ($preGI, $postGI) = get_neighbour($line);
                #get genome accesion
                my $acces = get_acces($line);
                $accesion_of{$acces}++;
                #Give uniq GI by passing acces after
                my $aGI = $GI . "/" .$acces;
                $cGI = $aGI;
                $seen_gi{$aGI}{acc}= $acces;
                $central_gi{$aGI}++;
                $basic_OG{$infile}{$aGI}++;
                my $apreGI = $preGI . "/" . $acces;
                $seen_gi{$aGI}{-1}= $apreGI;
                my $apostGI = $postGI . "/" .$acces;
                $seen_gi{$aGI}{1}= $apostGI;
                #get coordinate
                my ($min, $max) = get_coordinate($line);
                $seen_gi{$aGI}{min}= $min;
                $seen_gi{$aGI}{max}= $max;
                #get strand
                my $strand = get_strand($line);
                $strand_for{$aGI}= $strand;
                #get organism name
                my $org = get_org($line);
                $seen_gi{$aGI}{org}= $org;
                #get function
                my $product = get_product($line);
                $function_record{$GI} = $product;
                #get orthologous group name
                my $group = $infile;
                $seen_gi{$aGI}{group}= $infile;
                #get assembly
                $assembly_id{$GI} = $assembly;
                $assembly_acc{$acces} = $assembly;
                #construction of original mcl hash
                $origin_mcl{$infile}{$aGI}++;
            }
            else {
                $cSeq = $cSeq . $line;
                $seen_gi{$cGI}{seq}= $cSeq if eof;
            }
        }
    }
    return (\%accesion_of, \%seen_gi, \%central_gi, \%basic_OG, \%strand_for, \%function_record,
        \%assembly_id, \%assembly_acc, \%origin_mcl);
}

#Construct trio neighbour
sub trio_neighbours {
    tie my %central_gi, 'Tie::IxHash';
    %central_gi = %{$_[0]};
    my %seen_gi = %{$_[1]};

    my $count = 1;
    my %triplet_gi;

    my @centralgis = keys %central_gi;
    foreach my $centralgi (@centralgis){
        $triplet_gi{$count}{$centralgi}++;
        $triplet_gi{$count}{$seen_gi{$centralgi}{1}}++;
        $triplet_gi{$count}{$seen_gi{$centralgi}{-1}}++;
        $count++;
    }

    return %triplet_gi;
}

#while greedy loop to construct strect of neighbours
sub stretch_neighbour {
    my %triplet_gi = %{$_[0]};
    my %central_gi = %{$_[1]};
    my %seen_gi = %{$_[2]};

    my %done_of;
    my $events = 1;

    while ($events) {
        #reset the while condition
        $events = 0;
        my @tripletgis = sort keys %triplet_gi;
        MTRIPLET:
        for my $Mtripletgi (@tripletgis) {
            #don't merge same key
            next MTRIPLET if $done_of{$Mtripletgi};
            #deref
            my %master_gi = %{$triplet_gi{$Mtripletgi}};
            my @mastergis = keys %master_gi;
            STRIPLET:
            for my $Stripletgi (@tripletgis) {
                #don't treat same key or merge key
                next STRIPLET if $Mtripletgi == $Stripletgi;
                next STRIPLET if $done_of{$Stripletgi};
                #deref
                my %slave_gi = %{$triplet_gi{$Stripletgi}};
                #Test for merging
                for my $mastergi (@mastergis) {
                    my $Mchr = get_chr($mastergi);
                    #merge two triplet only if on the same chromosome
                    if (exists $slave_gi{$mastergi}) {
                        #add event for pass in while loop
                        $events++;
                        #all slave gi must be on the same chr to merge
                        my $chrtag = 1;
                        my @slavegis = keys %slave_gi;
                        for my $slavegi (@slavegis) {
                            my $Bchr = get_chr($slavegi);
                            unless ($Bchr == $Mchr) {
                                $chrtag = 0;
                            }
                        }
                        #add the second key to first one & avoid duplicate
                        if ($chrtag == 1) {
                            for my $slavegi (@slavegis) {
                                unless ($master_gi{$slavegi}) {
                                    $triplet_gi{$Mtripletgi}{$slavegi}++;
                                }
                            }
                        }
                        #delete the second hash in triplet_gi
                        delete $triplet_gi{$Stripletgi};
                        #count the second keys as done
                        $done_of{$Stripletgi}++;
                    }
                }
            }
        }
        #Conditional
        say "Neighbour loop events : " . $events;
    }

    # Taking central GI from the collection of neighbour gi
    tie my %neighbour_group, 'Tie::IxHash';
    tie my %neighbour_groupcopy, 'Tie::IxHash';
    my %inverse_group;
    my %neighbour_access;

    my @tripletgis = keys %triplet_gi;
    for my $tripletgi (@tripletgis) {
        #dref
        my %tripletgi_from = %{$triplet_gi{$tripletgi}};
        my @fromtripletgis = keys %tripletgi_from;
        my $size;
        for my $gi (@fromtripletgis) {
            if (exists $central_gi{$gi}) {
                $size++;
            }
        }
        for my $gi (@fromtripletgis) {
            #verification in GI central presence hash
            #summerize informations
            my $bacces = get_aAcc($gi);
            if (exists $central_gi{$gi}) {
                $neighbour_group{$size . "-" . $tripletgi}{GI}{$gi}++;
                $neighbour_groupcopy{$size . "-" . $tripletgi}{GI}{$gi}++;
                $neighbour_group{$size . "-" . $tripletgi}{OG}{$seen_gi{$gi}{group}}++;
                $neighbour_groupcopy{$size . "-" . $tripletgi}{OG}{$seen_gi{$gi}{group}}++;
                $inverse_group{$gi}= $size . "-" . $tripletgi;
                $neighbour_access{$seen_gi{$gi}{group}}{$bacces} = $size . "-" . $tripletgi;
            }
        }
    }

    return ( \%neighbour_group, \%neighbour_groupcopy, \%inverse_group, \%neighbour_access);
}

#while greedy lopp to make colinear of neighbours stretchs
sub make_colinear{
    tie my %neighbour_group, 'Tie::IxHash';
    %neighbour_group = %{$_[0]};
    my %accesion_of = %{$_[1]};
    my %neighbour_access = %{$_[2]};

    tie my %colinear_of, 'Tie::IxHash';
    my %master_group;
    #sort on hash size always largest group first
    my $colevents = 1;
    my $mastercheck;
    #array with al accesions
    my @accesions = sort keys %accesion_of;
    while ($colevents) {
        $colevents = 0;
        $mastercheck++;
        my %treat_of;
        my @neighbourgroups = reverse nsort keys %neighbour_group;
        MNEIGHBOUR:
        for my $mneighbourgroup (@neighbourgroups) {
            #when all master key loop end once, only consider master key, else slave key become master.
            if ($mastercheck > 1) {
                next MNEIGHBOUR unless ($master_group{$mneighbourgroup});
            }
            #skip if group already merge
            next MNEIGHBOUR if $treat_of{$mneighbourgroup};
            #get mcl from neighbour group
            my %mneighbourgroup_from = %{$neighbour_group{$mneighbourgroup}{OG}};
            my @frommneighbourgroups = sort keys %mneighbourgroup_from;
            my $mneighbourgroupvalue = keys %mneighbourgroup_from;
            #filter on the number of neighbour
            next MNEIGHBOUR if $mneighbourgroupvalue < $ARGV{'--minsize-neighbour'};
            SNEIGHBOUR:
            for my $sneighbourgroup (@neighbourgroups) {
                #skip if group already merge or same group
                next SNEIGHBOUR if $treat_of{$sneighbourgroup};
                next SNEIGHBOUR if $mneighbourgroup eq $sneighbourgroup;
                #skip if slave neighbourgroup is already contain in master neighbourgroup
                if ($master_group{$mneighbourgroup}) {
                    my %colinear_from = %{$colinear_of{$mneighbourgroup}};
                    next SNEIGHBOUR if $colinear_from{$sneighbourgroup};
                }
                #get mcl from neighbour group
                my %sneighbourgroup_from = %{$neighbour_group{$sneighbourgroup}{OG}};
                my @fromsneighbourgroups = sort keys %sneighbourgroup_from;
                my $sneighbourgroupvalue = keys %sneighbourgroup_from;
                #filter on the number of neighbour
                if ($sneighbourgroupvalue < $ARGV{'--minsize-neighbour'}) {
                    $treat_of{$sneighbourgroup}++;
                    next SNEIGHBOUR;
                }
                #Begin of the test of colinear (merge neighbour group when 1 OG in common)
                for my $mmcl (@frommneighbourgroups) {
                    if ($sneighbourgroup_from{$mmcl}) {
                        #only merge OG if all accesion are on the same chr in all OG
                        my $chrtag = 1;
                        if ($ARGV{'--colinear-grouping'} eq "separate") {
                            for my $accesion (@accesions) {
                                my $Mgroup = $neighbour_access{$mmcl}{$accesion};
                                if ($Mgroup) {
                                    my @Mgis = keys %{ $neighbour_group{$Mgroup}{"GI"} };
                                    #neighbour group on the same chr, take first to have chr id
                                    my $Mgi = $Mgis[0];
                                    my $Mchr = get_chr($Mgi);
                                    #compare this to slave neighbour group
                                    my @Bgis = keys %{ $neighbour_group{$sneighbourgroup}{"GI"} };
                                    my $Bgi = $Bgis[0];
                                    my $Bchr = get_chr($Bgi);
                                    if ($Bchr =! $Mchr) {
                                        $chrtag = 0;
                                    }
                                }
                            }
                        }
                        elsif ($ARGV{'--colinear-grouping'} eq "merge") {
                            $chrtag = 1;
                        }
                        if ($chrtag == 1) {
                            #Group the keys when 1 OG group in common
                            my @colinears = sort keys %colinear_of;
                            #merge to master group if necessary
                            for my $mcolinear (@colinears) {
                                my %colinear_from = %{$colinear_of{$mcolinear}};
                                my @fromcolinears = sort keys %colinear_from;
                                #Query if an master group already contain sneighbourgroup
                                if ($colinear_from{$sneighbourgroup}) {
                                    #change master group
                                    $colinear_of{$mneighbourgroup}{$mneighbourgroup}++;
                                    $master_group{$mneighbourgroup}++;
                                    #add all old master group to new one
                                    for my $scolinear (@fromcolinears) {
                                        $colinear_of{$mneighbourgroup}{$scolinear}++;
                                        $treat_of{$scolinear}++;
                                        #merge OG group to the new master neighbour group
                                        my %neighbourgroup_from = %{$neighbour_group{$scolinear}{OG}};
                                        my @fromneighbourgroups = sort keys %neighbourgroup_from;
                                        for my $OG (@fromneighbourgroups) {
                                            $neighbour_group{$mneighbourgroup}{OG}{$OG}++;
                                        }
                                    }
                                #delete old master neighbour group, now individual slave
                                delete $colinear_of{$mcolinear};
                                delete $master_group{$mcolinear};
                                $colevents++;
                                next SNEIGHBOUR;
                                }
                            }
                            #if colinear_from don't contain sneighbourgroup
                            #treat master group
                            $colinear_of{$mneighbourgroup}{$mneighbourgroup}++ unless $colinear_of{$mneighbourgroup};
                            $master_group{$mneighbourgroup}++ unless $master_group{$mneighbourgroup};
                            #treat slave group
                            $colinear_of{$mneighbourgroup}{$sneighbourgroup}++;
                            $treat_of{$sneighbourgroup}++;
                            #merge OG group to the master neighbour group
                            for my $OG (@fromsneighbourgroups) {
                                $neighbour_group{$mneighbourgroup}{OG}{$OG}++;
                            }
                            $colevents++;
                            next SNEIGHBOUR;
                        }
                    }
                }
            }
        }
    }
    #remember gi implicate in colinear group (use in sorting by reversal)
    my %neighbour_gi;
    my @colinearofs = keys %colinear_of;
    for my $colinearof (@colinearofs) {
        my %colinear_from = %{$colinear_of{$colinearof}};
        my @colinearfroms = keys %colinear_from;
        for my $colinearfrom (@colinearfroms) {
            my %chunks_of = %{$neighbour_group{$colinearfrom}{"GI"}};
            my @chunks = keys %chunks_of;
            for my $gi (@chunks) {
                $neighbour_gi{$gi}++;
            }
        }
    }

    return ( \%neighbour_group, \%colinear_of, \%neighbour_gi, \@accesions);

}
#format colinear group
sub format_colinear {
    my $colinear = $_[0];
    my %colinear_of = %{$_[1]};
    my %OG_ofcopy = %{$_[2]};
    my %seen_gi = %{$_[3]};
    tie my %neighbour_group, 'Tie::IxHash';
    %neighbour_group = %{$_[4]};
    my %OGlist_of = %{$_[5]};

    #take all neighbour group for a master colinear_of group
    my %colinear_from = %{$colinear_of{$colinear}};
    #for every OG group from %colinear_from take the GI in %OG_of
    my %OG_of;
    my @fromcolinears = sort keys %colinear_from;
    for my $ccolinear (@fromcolinears) {
        my %neighbourgroup_from = %{$neighbour_group{$ccolinear}{GI}};
        my @fromneighbourgroups = sort keys %neighbourgroup_from;
        for my $gi (@fromneighbourgroups) {
            $OG_of{$seen_gi{$gi}{group}}{$gi}++;
            #copy %OG_of to keep full version after further deletion in OG_of
            $OG_ofcopy{$seen_gi{$gi}{group}}{$gi}++;
            $OGlist_of{$seen_gi{$gi}{group}}++;
        }
    }

    return (\%OG_of, \%OG_ofcopy, \@fromcolinears, \%OGlist_of);
}

#chech presence of genomes in colinear group
sub colinear_presence {
    my @accesions = @{$_[0]};
    my %OG_of = %{$_[1]};
    my $threshold = $_[2];
    my %accesion_of = %{$_[3]};

    #Treshold on number of genomes present in the all colinear group
    my %genome_presence;
    #pass by all genome accesion to see if there a in colinear group
    for my $accesion (@accesions) {
        my @OGs = sort keys %OG_of;
        for my $OG (@OGs) {
            my %OG_from = %{$OG_of{$OG}};
            my @fromOGs = sort keys %OG_from;
            #check if gi conatin the accesion number, gi is concat of gi & accesion
            for my $gi (@fromOGs) {
                if ($gi =~ $accesion) {
                    $genome_presence{$accesion}++
                }
            }
        }
    }
    #calcul on the number of genome, passing by treshold
    my $presence = keys %genome_presence;
    my $accesion = keys %accesion_of;
    my $presentpercent = (($presence/$accesion)*100);
    #next COLINEAR if $presentpercent < $ARGV{'--mingenome-colinear'};
    my $statut;
    if ($presentpercent < $threshold) {
        $statut = 'next';
    }
    else {
        $statut = 'pass';
    }

    return $statut;
}

#order the colinear group
sub colinear_ordering {

    my $colinear =  $_[0];
    my @fromcolinears = @{$_[1]};
    tie my %neighbour_group, 'Tie::IxHash';
    tie my %neighbour_groupcopy, 'Tie::IxHash';
    %neighbour_groupcopy = %{$_[2]};
    %neighbour_group = %{$_[3]};
    my %OG_of = %{$_[4]};
    my %seen_gi = %{$_[5]};
    my $iteration = $_[6];

    my @orderedOGs;
    #boucle while for order
    my $check = 1;
    my %done_mcl;
    my @orderacces;
    #my $blockkey;


    while ($check) {
        #reset while condition
        $check = 0;

        #Make %colinear_size who contain for each colinear group the number of untreat OGs
        my %colinear_size;
        for my $fromcolinear (@fromcolinears) {
            #take the OGs from the copy of %neighbour_group
            my %neighbourgroupcopy_from = %{$neighbour_groupcopy{$fromcolinear}{OG}};
            my @fromneighbourgroupcopys = sort keys %neighbourgroupcopy_from;
            my $number;
            #calcul the number og OGs in %OG_of for $fromcolinear
            for my $OG (@fromneighbourgroupcopys) {
                if ($OG_of{$OG}) {
                    $number++;
                }
            }
            if ($number) {
                $colinear_size{$fromcolinear}= $number;
            }
            else {
                $colinear_size{$fromcolinear}= 0;
            }
        }
        #make an array
        my @colinearsizes;
        for my $colinear (sort { $colinear_size{$b} <=> $colinear_size{$a} || $a cmp $b } keys %colinear_size) {
            push @colinearsizes, $colinear;
        }
        #take the xeme (iteration) to ordonnate
        my $iter = 0;
        while ($iter < $iteration) {
            my $first = shift @colinearsizes;
            push @colinearsizes, $first;
            $iter++;
        }

        #Take the neighbour group of colinear who have the highest number of untreat OGs
        #changer size pr order
        SIZE:
        #for my $colinear (sort { $colinear_size{$b} <=> $colinear_size{$a} || $a cmp $b } keys %colinear_size) {
        for my $colinear (@colinearsizes) {
            my %neighbourgroup_from = %{$neighbour_group{$colinear}{GI}};
            my %order_of;
            my @fromneighbourgroups = sort keys %neighbourgroup_from;
            #construct %order_of with gene position on gbk file for each gi
            for my $gi (@fromneighbourgroups) {
                my $order = get_order($gi);
                $order_of{$order}{$gi}++;
            }
            #define ordonate array according the gene position in %order_of
            my @neighbourorders;
            my @orders = sort {$a <=> $b} keys %order_of;
            for my $order (@orders) {
                my %order_from = %{$order_of{$order}};
                my @fromorders = sort keys %order_from;
                my $gi = $fromorders[0];
                push @neighbourorders, $gi;
            }
            my $neighboursize = scalar @neighbourorders;

            #Firt ordering of @ordered
            unless (@orderedOGs) {
                #Pass by order gi ($neighbourorder) et take the OG conatining this GI for ordering
                ORDER:
                for my $neighbourorder (@neighbourorders) {
                    #loop in all OG to assess presence of gi $neighbourorder
                    my @OGs = sort keys %OG_of;
                    for my $OG (@OGs) {
                        my %OG_from = %{$OG_of{$OG}};
                        if ($OG_from{$neighbourorder}) {
                            #init while condition
                            $check++;
                            #order the OG array
                            push @orderedOGs, $OG;
                            #delete in OG_of for further loop
                            delete $OG_of{$OG};
                            $done_mcl{$OG}++;
                            #@orderacces to keep the order of first ordering genome
                            my $doneacc = get_aAcc($neighbourorder);
                            #$doneacc =~ s/\d+//;
                            my %order_of = map { $_ => 1 } @orderacces;
                            unless ($order_of{$doneacc}) {
                                push @orderacces, $doneacc;
                            }
                            next ORDER;
                        }
                    }
                }
                #comlete ordering of colinear group with highest number of untreat OGs
                #Pass in the beginning of loop to take the next colinear group
                last SIZE;
            }
            #reordering of @ordered
            else {
                #Pass by order gi ($neighbourorder) et take the OG conatining this GI for ordering
                REORDER:
                for my $neighbourorder (@neighbourorders) {
                    #loop in all OG to assess presence of gi $neighbourorder
                    my @OGs = sort keys %OG_of;
                    for my $OG (@OGs) {
                        my %OG_from = %{$OG_of{$OG}};
                        if ($OG_from{$neighbourorder}) {
                            #skip if OG group if already in @ordered
                            next REORDER if $done_mcl{$OG};
                            #Take the position of the gi in the neighbour order gi @neighbourorders
                            my $index = first_index { $_ eq $neighbourorder } @neighbourorders;
                            if ($index == 0) {
                                #Case if the gi is at first position in @neighbourorders
                                #look if right next OG alreay in @ordered
                                #from nextgi to nextOG
                                my $next = $index + 1;
                                my $nextgi = $neighbourorders[$next];
                                my $nextOG = $seen_gi{$nextgi}{group};
                                #take the index of the next OG
                                my $globalindex = first_index { $_ eq $nextOG } @orderedOGs;
                                #assess if next OG is present
                                if ($globalindex != -1) {
                                    #init while condition
                                    $check++;
                                    #insert before next OG
                                    my $insertindex = $globalindex;
                                    splice @orderedOGs, $insertindex, 0, $OG;
                                    #delete in OG_of for further loop
                                    delete $OG_of{$OG};
                                    $done_mcl{$OG}++;
                                    #@orderacces to keep the order of first ordering genome
                                    my $doneacc = get_aAcc($neighbourorder);
                                    #$doneacc =~ s/\d+//;
                                    my %order_of = map { $_ => 1 } @orderacces;
                                    unless ($order_of{$doneacc}) {
                                        #### Insert first
                                        push @orderacces, $doneacc;
                                    }
                                    next REORDER;
                                }
                            }
                            elsif ($index == $neighboursize -1) {
                                #Case if the gi is at last position in @neighbourorders
                                #look if left pre OG alreay in @ordered
                                #from pregi to preOG
                                my $pre = $index - 1;
                                my $pregi = $neighbourorders[$pre];
                                my $preOG = $seen_gi{$pregi}{group};
                                #take the index of pre OG
                                my $globalindex = first_index { $_ eq $preOG } @orderedOGs;
                                #assess if pre OG is present
                                if ($globalindex != -1) {
                                    #init while condition
                                    $check++;
                                    #insert after pre OG
                                    my $insertindex = $globalindex + 1;
                                    splice @orderedOGs, $insertindex, 0, $OG;
                                    #delete in OG_of for further loop
                                    delete $OG_of{$OG};
                                    $done_mcl{$OG}++;
                                    #@orderacces to keep the order of first ordering genome
                                    my $doneacc = get_aAcc($neighbourorder);
                                    #$doneacc =~ s/\d+//;
                                    my %order_of = map { $_ => 1 } @orderacces;
                                    unless ($order_of{$doneacc}) {
                                        #### Insert last
                                        push @orderacces, $doneacc;
                                    }
                                    next REORDER;
                                }
                            }
                            else {
                                #Case of non extreme gi in @neighbourorders
                                #look if left pre OG alreay in @ordered
                                #from pregi to preOG
                                my $pre = $index - 1;
                                my $pregi = $neighbourorders[$pre];
                                my $preOG = $seen_gi{$pregi}{group};
                                #take the index of pre OG
                                my $leftindex = first_index { $_ eq $preOG } @orderedOGs;
                                #look if right next OG alreay in @ordered
                                #from nextgi to nextOG
                                my $next = $index + 1;
                                my $nextgi = $neighbourorders[$next];
                                my $nextOG = $seen_gi{$nextgi}{group};
                                #take the index of next OG
                                my $rightindex = first_index { $_ eq $nextOG } @orderedOGs;
                                #assess if pre OG is present
                                if ($leftindex != -1) {
                                    #init while condition
                                    $check++;
                                    #insert after pre OG
                                    my $insertindex = $leftindex + 1;
                                    splice @orderedOGs, $insertindex, 0, $OG;
                                    #delete in OG_of for further loop
                                    delete $OG_of{$OG};
                                    $done_mcl{$OG}++;
                                    #@orderacces to keep the order of first ordering genome
                                    my $doneacc = get_aAcc($neighbourorder);
                                    #$doneacc =~ s/\d+//;
                                    my %order_of = map { $_ => 1 } @orderacces;
                                    unless ($order_of{$doneacc}) {
                                        #### Insert middle A
                                        push @orderacces, $doneacc;
                                    }
                                    next REORDER;
                                }
                                #assess if next OG is present
                                elsif ($rightindex != -1) {
                                    #init while condition
                                    $check++;
                                    #insert before pre OG
                                    my $insertindex = $rightindex;
                                    splice @orderedOGs, $insertindex, 0, $OG;
                                    #delete in OG_of for further loop
                                    delete $OG_of{$OG};
                                    $done_mcl{$OG}++;
                                    #@orderacces to keep the order of first ordering genome
                                    my $doneacc = get_aAcc($neighbourorder);
                                    #$doneacc =~ s/\d+//;
                                    my %order_of = map { $_ => 1 } @orderacces;
                                    unless ($order_of{$doneacc}) {
                                        #### Insert middle B
                                        push @orderacces, $doneacc;
                                    }
                                    next REORDER;
                                }
                            }
                        }
                    }
                }
                if ($check > 0) {
                    ##Pass in the beginning of loop to take the next colinear group if something in while lopp
                    #### Ordering colinear group
                    last SIZE;

                }
            }
        }
    }

    return (\@orderedOGs, \@orderacces, \%OG_of, \%neighbour_group, \%neighbour_groupcopy )
}

#make ordered array of access
sub make_orderacces {
    my %accesion_of = %{$_[0]};
    my @orderacces = @{$_[1]};

    #make an array with the first accesion as the first ordering genome, the next in a sort order
    my @accesnumber = sort keys %accesion_of;
    my %order_of = map { $_ => 1 } @orderacces;
    for my $acces (@accesnumber) {
        next if $order_of{$acces};
        push @orderacces, $acces;
    }
    @accesnumber = @orderacces;

    return (\@accesnumber, \%order_of);
}

#sorting by reversal
sub sorting_byreversal {

    my @accesnumber = @{$_[0]};
    my %basic_OG = %{$_[1]};
    my %neighbour_gi = %{$_[2]};
    my %OG_ofcopy = %{$_[3]};
    my @orderedOGs = @{$_[4]};
    my $reverse = $_[5];
    my %meta_acc= %{$_[6]};

    #classical orientation
    my %absence_of;
    my %event_of;
    my %revert_of;
    my %unfound_of;
    my %end_record;
    my %limit_of;

    if ($reverse == 1) {
        @orderedOGs = reverse @orderedOGs;
    }

    #count unfound in neighbouring before reversal
    my %count_unfound;
    PRES:
    for my $acces (@accesnumber){
        next PRES if $meta_acc{$acces};
        for my $MorderedOG (@orderedOGs) {
            #get informations in GI in basic_OG; raw informations
            my %Mfrombasic_OG = %{$basic_OG{$MorderedOG}};
            my @Mfrombasic_OGs = sort keys %Mfrombasic_OG;
            my $MfrombasicOGssize = scalar @Mfrombasic_OGs;
            #assess the presence in the basic_OG
            my $accespass = 0;
            for my $MBgi (@Mfrombasic_OGs) {
                if ($MBgi =~ $acces) {
                    #genome present
                    #check if still present after neighbouring
                    unless ($neighbour_gi{$MBgi}) {
                        $unfound_of{$MorderedOG}++;
                        $unfound_of{$acces}++;
                        $count_unfound{$MorderedOG}{$acces}++;
                    }
                }
                else {
                    $accespass++;
                 }
            }
        }
    }

    my $reverkey;
    REVERSAL:
    for my $acces (@accesnumber){
        next REVERSAL if $meta_acc{$acces};
        my %done_OG;
        for my $MorderedOG (@orderedOGs) {
            $done_OG{$MorderedOG}++;
            #take gi of OG
            my %MOGofcopy_from = %{$OG_ofcopy{$MorderedOG}};
            my @MfromOGofcopys = sort keys %MOGofcopy_from;

            #assess the presence in the basic_OG
            my $accespass = 0;
            #get informations in GI in basic_OG; raw informations
            my %Mfrombasic_OG = %{$basic_OG{$MorderedOG}};
            my @Mfrombasic_OGs = sort keys %Mfrombasic_OG;
            my $MfrombasicOGssize = scalar @Mfrombasic_OGs;
            for my $MBgi (@Mfrombasic_OGs) {
                if ($MBgi =~ $acces) {
                    #genome present
                }
                else {
                    $accespass++;
                 }
            }
            if ($accespass == $MfrombasicOGssize) {
                #genomes absent
                $absence_of{$MorderedOG}++;
                $absence_of{$acces}++;
            }
            else {
                $absence_of{$MorderedOG} = 0 unless $absence_of{$MorderedOG};
            }
            #asses the order in OG/inversion process
            #as order is assess from first OG, this one never have an inversion event
            unless ($event_of{$MorderedOG}) {
                my $firstOG = $orderedOGs[0];
                if ($MorderedOG eq $firstOG) {
                    $event_of{$MorderedOG} = 0;
                    $unfound_of{$MorderedOG} = 0 unless $unfound_of{$MorderedOG};
                }
            }
            #define Master order and exepected order +1/-1
            my $Morder = 0;
            #define Master order if inversion process record
            my $Mrev = $revert_of{$acces}{$MorderedOG};
            if ($Mrev) {
                $Morder = $revert_of{$acces}{$MorderedOG};
            }
            else {
                for my $Mgi (@MfromOGofcopys) {
                    if ($Mgi =~ $acces) {
                        my $basegi = $Mgi;
                        $Morder = get_order($Mgi);
                    }
                }
            }
            my $Morderadd = $Morder + 1 if $Morder;
            my $Mordermin = $Morder - 1 if $Morder;
            #slave part of OG if a master order is define
            my $unorder;
            my $tag = 0;
            if ($Morder) {
                SORTING:
                for my $SorderedOG (@orderedOGs) {
                    next SORTING if $done_OG{$SorderedOG};
                    #take gi of OG
                    my %SOGofcopy_from = %{$OG_ofcopy{$SorderedOG}};
                    my @SfromOGofcopys = sort keys %SOGofcopy_from;
                    #take order
                    my $Sorder = 0;
                    my $Srev = $revert_of{$acces}{$SorderedOG};
                    if ($Srev) {
                        $Sorder = $revert_of{$acces}{$SorderedOG};
                    }
                    else {
                        for my $Sgi (@SfromOGofcopys) {
                            if ($Sgi =~ $acces) {
                                $Sorder = get_order($Sgi);
                            }
                        }
                    }
                    #asses order succesion
                    if ( ($Sorder == $Morderadd) || ($Sorder == $Mordermin)) {
                        unless ($unorder) {
                            #no inversion/ no unorder tag activate
                            my $index = first_index { $_ eq $MorderedOG } @orderedOGs;
                            $index++;
                            my $nextOG = $orderedOGs[$index];
                            $event_of{$nextOG}= 0 unless $event_of{$nextOG};
                            $unfound_of{$nextOG} = 0 unless $unfound_of{$nextOG};
                            last SORTING;
                        }
                        elsif ($unorder){
                            #inversion/ tag unorder activate and expected order found
                            #define first OG, proximal of inversion
                            my $index = first_index { $_ eq $MorderedOG } @orderedOGs;
                            $index++;
                            my $nextOG = $orderedOGs[$index];

                            #If proximal OG do not contain access, out of sorting : no inversion if one of both end is missing
                            #If distal not present, class as missing already done
                            my %LOGofcopy_from = %{$OG_ofcopy{$nextOG}};
                            my @LfromOGofcopys = sort keys %LOGofcopy_from;
                            my $loopacces = 0;
                            my $loopsize = scalar @LfromOGofcopys;
                            LOOPACCES:
                            for my $Lgi (@LfromOGofcopys) {
                                if ($Lgi =~ $acces) {
                                    last LOOPACCES;
                                }
                                else {
                                    $loopacces++;
                                }
                            }
                            if ($loopacces == $loopsize) {
                                #proximal OG of inversion do not contain the acces
                                last SORTING;
                            }

                            #define last OG, distal of inversion
                            my $distalindex = first_index { $_ eq $SorderedOG } @orderedOGs;
                            my $distalOG = $orderedOGs[$distalindex];
                            #inversion record only once if both end are the same
                            $end_record{$nextOG}{$distalOG}++;
                            unless($end_record{$nextOG}{$distalOG} > 1) {
                                $event_of{$nextOG}++;
                            }
                            $event_of{$acces}++;
                            $unfound_of{$nextOG} = 0 unless $unfound_of{$nextOG};
                            #inversion process
                            #reverse the block
                            my $invindex = first_index { $_ eq $SorderedOG } @orderedOGs;
                            my $upindex = $invindex + 1;
                            #define bounds of invertion for block creation
                            $reverkey++;
                            #register previous inversion, load temporaire
                            my $registerindex = $index;
                            my %previous_event;
                            while ($index <= $invindex) {
                                my $distalindex = $upindex - 1;
                                $upindex = $distalindex;
                                my $distalOG = $orderedOGs[$distalindex];
                                if ($revert_of{$acces}{$distalOG}) {
                                    my $distalorder = $revert_of{$acces}{$distalOG};
                                    $previous_event{$acces}{$distalOG} = $distalorder;
                                }
                                $index++;
                            }
                            #reverse process
                            my $reverloop;
                            my $proxyOG;
                            $index = $registerindex;
                            $invindex = first_index { $_ eq $SorderedOG } @orderedOGs;
                            $upindex = $invindex + 1;
                            while ($index <= $invindex) {
                                $reverloop++;
                                #replace distal order by proxi order until distal OG become proxy OG, for current acces
                                #treat distal OG
                                my $distalindex = $upindex - 1;
                                $upindex = $distalindex;
                                my $distalOG = $orderedOGs[$distalindex];
                                $proxyOG = $distalOG;
                                my $distalorder;
                                my %wOGofcopy_from = %{$OG_ofcopy{$distalOG}};
                                my @wfromOGofcopys = sort keys %wOGofcopy_from;
                                unless ($previous_event{$acces}{$distalOG}) {
                                    for my $wgi (@wfromOGofcopys) {
                                        if ($wgi =~ $acces) {
                                            $distalorder = get_order($wgi);
                                        }
                                    }
                                }
                                if ($previous_event{$acces}{$distalOG}) {
                                    $distalorder = $previous_event{$acces}{$distalOG};
                                }
                                #treat OG if absence
                                $distalorder = 1 unless $distalorder;
                                #treat proxy OG
                                $nextOG = $orderedOGs[$index];
                                $revert_of{$acces}{$nextOG}= $distalorder;
                                #define bounds of invertion
                                if ($reverloop == 1) {
                                    $limit_of{$reverkey}{$distalOG} = "distal";
                                }
                                else {
                                    $limit_of{$reverkey}{$distalOG} = "open";
                                }
                                $index++;
                            }
                            #define bounds of invertion
                            $limit_of{$reverkey}{$proxyOG} = "proxy";
                            last SORTING;
                        }
                    }
                    else {
                        #activate unorder tag
                        $unorder++;
                        #if arrive at the end of block without found, tag unfound activate if not missing
                        my $size = scalar @orderedOGs;
                        my $index = $size - 1;
                        my $lastOG = $orderedOGs[$index];
                        #if genomes present in basic OG, activate tag
                        my %Sfrombasic_OG = %{$basic_OG{$SorderedOG}};
                        my @Sfrombasic_OGs = sort keys %Sfrombasic_OG;
                        for my $SBgi (@Sfrombasic_OGs) {
                            if ($SBgi =~ $acces) {
                            #genome present
                            $tag++
                            }
                        }
                        if (($SorderedOG eq $lastOG) && ($tag > 0)) {
                            my $index = first_index { $_ eq $MorderedOG } @orderedOGs;
                            $index++;
                            my $nextOG = $orderedOGs[$index];
                            #count unfound activate unless already done for this acces in this OG
                            unless ($count_unfound{$nextOG}{$acces}) {
                                $unfound_of{$nextOG}++ unless $unfound_of{$nextOG};
                                $unfound_of{$acces}++ unless $unfound_of{$nextOG};
                                $event_of{$nextOG} = 0 unless $event_of{$nextOG};
                            }
                        }
                    }
                }
            }
        }
    }
    return (\%absence_of, \%event_of, \%revert_of, \%unfound_of, \%end_record, \%limit_of, \%OG_ofcopy, \@orderedOGs);
}

#syntenic block creation according to threshold
sub block_creation {

    my @orderedOGs = @{$_[0]};
    my %absence_of = %{$_[1]};
    my %event_of = %{$_[2]};
    my %revert_of = %{$_[3]};
    my %unfound_of = %{$_[4]};
    my %end_record = %{$_[5]};
    my %limit_of = %{$_[6]};
    my $blockkey = $_[7];
    my $missthreshold = $_[8];
    my $invthreshold = $_[9];
    my $sizethreshold = $_[10];
    my %comparing_block = %{$_[11]};
    my %rcomparing_block = %{$_[12]};
    my $colinear = $_[13];
    my $reverse = $_[14];

    #initiate values
    my $missing = 0;
    my $unfound = 0;
    my $inversion = 0;
    my @blocks;
    my @tempblocks;

    #loop in array by number of OG
    my $colsize = scalar @orderedOGs;
    my $lastindex = $colsize -1;
    #loop control
    my $stop = 0;
    my $startblock;
    #event control
    my $openevent = 0;
    my @openevents;
    my $endevent = 0;
    my $startingevent = 0;
    my $endtagpass = 0;
    my $lastog;

    for my $j (0..$colsize-1) {
        $startblock++;
        my $endtag;
        my $currentopen = 0;
        #push the previous (if not first) OG if it pass treshold
        #push also if it's the first OG of iteration and if pass treshold
        if ($stop > 0) {
            my $previousOG = $orderedOGs[$j-1];
            my $previousmissing = $absence_of{$previousOG};
            my $previousunfound = $unfound_of{$previousOG};
            my $previousinversion = $event_of{$previousOG};
            if (($previousmissing <= $missthreshold) && ($previousunfound<= $missthreshold) &&
                ($previousinversion <= $invthreshold)) {
                #if current inversion open, push in @tempblocks
                if (($openevent > 0) && ($startingevent == $openevent)) {
                    push @tempblocks, $previousOG;
                }
                #if no current inversion open, push in @blocks
                if ($openevent == 0) {
                    push @blocks, $previousOG;
                }
                $missing = $missing + $previousmissing;
                $inversion = $inversion + $previousinversion;
                $unfound =  $unfound + $previousunfound;
            }
            $stop = 0;
        }
        #take value of inversion/missing/unfound of current OG and add to some of block
        #when first OG, just look at missing : first OG or beggining of iteration
        my $orderedOG = $orderedOGs[$j];
        $missing = $missing + $absence_of{$orderedOG};
        $inversion = $inversion + $event_of{$orderedOG};
        $unfound =  $unfound + $unfound_of{$orderedOG};

        #define opening of event
        if ($endevent > 0) {
            $openevent--;
            $endevent--;
            $startingevent--;
        }
        my @limits = keys %limit_of;
        my $openstatus;
        for my $limit (@limits) {
            my $status = $limit_of{$limit}{$orderedOG};
            if ($status) {
                if ($status eq "proxy") {
                    $startingevent++;
                    $currentopen++;
                }
                if (($status eq "proxy") || ($status eq "open")) {
                    $openevent++ unless grep{$_ == $limit} @openevents;
                    push(@openevents, $limit) unless grep{$_ == $limit} @openevents;
                }
                elsif ($status eq "distal") {
                    #due to alternate starting point, can arrive distal as first occurence
                    if ($openevent > 0) {
                        $endevent++;
                        #delete open invertion
                        my $limitindex = first_index { $_ eq $limit } @openevents;
                        splice(@openevents, $limitindex, 1);
                    }
                }
            }
        }

        #check threshold
        if ($j == $lastindex) {
            #if arrive at last OG, record it and proceed threshold evulation
            $lastog++;
            if (($missing <= $missthreshold) && ($unfound <= $missthreshold)
            && ($inversion <= $invthreshold) ) {
                #if current inversion open, push in @tempblocks
                if (($openevent > 0) && ($startingevent == $openevent) ) {
                    push @tempblocks, $orderedOG;
                }
                #if no current inversion open, push in @blocks
                if ($openevent == 0) {
                    my $tempsize = scalar @tempblocks;
                    if ($tempsize > 0) {
                        push @blocks, @tempblocks;
                        @tempblocks = ();
                    }
                push @blocks, $orderedOG;
                }
            }
            else {
                #end a block
                $endtag++;
            }
        }
        elsif (($missing <= $missthreshold) && ($unfound <= $missthreshold)
        && ($inversion <= $invthreshold) ) {
            #classical thresold evaluation not last index
            #if current inversion open, push in @tempblocks
            if (($openevent > 0) && ($startingevent == $openevent)) {
                push @tempblocks, $orderedOG;
            }
            #if no current inversion open, push in @blocks
            if ($openevent == 0) {
                my $tempsize = scalar @tempblocks;
                if ($tempsize > 0) {
                    push @blocks, @tempblocks;
                    @tempblocks = ();
                }
                push @blocks, $orderedOG;
            }
        }
        else {
            #end a block
            $endtag++;
        }
        if ($endtag || $lastog) {
            #if close a block, load @tempblocks in @blocks
            #event minder event of current OG not take in charge
            my $endtagevent = $openevent - $currentopen;
            if ($endtagevent == 0) {
                my $tempsize = scalar @tempblocks;
                if ($tempsize > 0) {
                        push @blocks, @tempblocks;
                }
            }
            #empty @tempblocks
            @tempblocks = ();
            #activate stop tag, to load current OG at next step
            $stop++;
            #the treshold not pass, look if @blocks pass the size treshold
            my $blocksize = scalar @blocks;
            if ($blocksize >= $sizethreshold) {
                #register the block if pass size threshold
                #define partial key for block
                $blockkey++;
                if ($reverse == 0) {
                    #record events informations
                    #all event - current event because stop at this position (unless last position)
                    my $realmissing;
                    my $realinversion;
                    my $realunfound;
                    if ($endtag) {
                        $realmissing = $missing - $absence_of{$orderedOG};
                        $realinversion = $inversion - $event_of{$orderedOG};
                        $realunfound = $unfound - $unfound_of{$orderedOG};
                    }
                    else {
                        $realmissing = $missing;
                        $realinversion = $inversion;
                        $realunfound = $unfound;
                    }
                    $event_record{"$colinear-$blockkey"}{"missing"}= $realmissing;
                    $event_record{"$colinear-$blockkey"}{"inversion"}= $realinversion;
                    $event_record{"$colinear-$blockkey"}{"unfound"}= $realunfound;
                    $event_record{"$colinear-$blockkey"}{"size"}= $blocksize;
                    #record block
                    for my $OG (@blocks) {
                        push(@{$comparing_block{"$colinear-$blockkey"}}, $OG);
                    }
                }
                elsif ($reverse == 1) {
                    #record events informations
                    #all event - current event because stop at this position (unless last position)
                    my $realmissing;
                    my $realinversion;
                    my $realunfound;
                    if ($endtag) {
                        $realmissing = $missing - $absence_of{$orderedOG};
                        $realinversion = $inversion - $event_of{$orderedOG};
                        $realunfound = $unfound - $unfound_of{$orderedOG};
                    }
                    else {
                        $realmissing = $missing;
                        $realinversion = $inversion;
                        $realunfound = $unfound;
                    }
                    $event_record{"$colinear-$blockkey"}{"missing"}= $realmissing;
                    $event_record{"$colinear-$blockkey"}{"inversion"}= $realinversion;
                    $event_record{"$colinear-$blockkey"}{"unfound"}= $realunfound;
                    $event_record{"$colinear-$blockkey"}{"size"}= $blocksize;
                    #record block
                    for my $OG (@blocks) {
                        push(@{$rcomparing_block{"$colinear-$blockkey"}}, $OG);
                    }
                }
            }
            #reset values for treshold
            $missing = 0;
            $inversion = 0;
            $unfound = 0;
            @blocks = ();
        }
    }

    return (\%comparing_block, \%rcomparing_block, \%event_record, $blockkey, \@orderedOGs);
}

#compare the two orientation
sub compare_orientation {

    my %comparing_block = %{$_[0]};
    my %rcomparing_block = %{$_[1]};
    my %event_record = %{$_[2]};
    my %syntenic_block = %{$_[3]};
    my $syngene = $_[4];

    #define default orientation as classical
    my $choicesize = 0;
    my $choice = "classical";
    for my $comparekey (keys %comparing_block) {
        my $classicsize = $event_record{"$comparekey"}{"size"};
        if ($classicsize > $choicesize) {
            $choicesize = $classicsize;
        }
    }
    #check if reverse do it better
    for my $rcomparekey (keys %rcomparing_block) {
        my $reversesize = $event_record{"$rcomparekey"}{"size"};
        if ($reversesize > $choicesize) {
            $choice = "reverse";
            $choicesize = $reversesize;
        }
    }
    #register best syntenic blocks
    if ($choice eq "classical") {
        for my $comparekey (keys %comparing_block) {
            my @compareblocks = @{$comparing_block{$comparekey}};
            for my $compareblock (@compareblocks) {
                push(@{$syntenic_block{"$comparekey"}}, $compareblock);
                $syngene++;
            }
        }
        #classical mode for theses blocks, put @orientedOGs in classical orientation
        #@orderedOGs = reverse @orderedOGs;
    }
    elsif ($choice eq "reverse") {
        for my $rcomparekey (keys %rcomparing_block) {
            my @rcompareblocks = @{$rcomparing_block{$rcomparekey}};
            for my $rcompareblock (@rcompareblocks) {
                push(@{$syntenic_block{"$rcomparekey"}}, $rcompareblock);
                $syngene++;
            }
        }
    }

    return (\%syntenic_block, \%event_record, $syngene, $choice);
}

#compare the iteration in ordering colinear group
sub compare_iter {

    tie my %orderiter_of, 'Tie::IxHash';
    %orderiter_of = %{$_[0]};

    #define an array ordonate by decresing syngene number or by iteration when equality
    my @iters;
    for my $iter ((sort {$a <=> $b} values $orderiter_of{'syngen'}) || (sort {$a <=> $b} values $orderiter_of{'iter'})) {
        push @iters, $iter;
    }
    #return best iteration
    my $bestiter = shift @iters;
    my $block = $orderiter_of{'synblock'}{$bestiter};
    my $event = $orderiter_of{'event'}{$bestiter};
    my $genenum = $orderiter_of{'syngen'}{$bestiter};
    my $orientation = $orderiter_of{'orientation'}{$bestiter};

    return ($block, $event, $genenum, $orientation);
}

#print the gbk et coo map
sub print_map {

    my $colinear = $_[0];
    my @accesnumber = @{$_[1]};
    my %assembly_acc = %{$_[2]};
    my %order_of = %{$_[3]};
    my %OG_ofcopy = %{$_[4]};
    my %strand_for = %{$_[5]};
    my %seen_gi = %{$_[6]};
    my $cooout = $_[7];
    my $gbkout = $_[8];
    my $orientation = $_[9];
    my @orderedOGs = @{$_[10]};

    #reverse the orderedOGs if choice is reverse
    if ($orientation eq 'reverse') {
        @orderedOGs = reverse @orderedOGs;
    }
    #print @ordered in aligment mode (genome by genome)
    print {$cooout} ">" . "colinear group" . $colinear . "\n";
    print {$gbkout} ">" . "colinear group" . $colinear . "\n";

    for my $acces (@accesnumber) {
        #Print M (master) information for the ordering genome
        my $assembly = $assembly_acc{$acces};
        $assembly =~ s/\ /\_/g;
        if ($order_of{$acces}) {
            print {$cooout} "-" . $assembly . "-" . "M" . "\t";
            print {$gbkout} "-" . $assembly . "-" . "M" . "\t";
        }
        else {
            print {$cooout} "-" . $assembly . "\t";
            print {$gbkout} "-" . $assembly . "\t";
        }
        #print OG by OG for column
        for my $orderedOG (@orderedOGs) {
            #Get the selected gi for the OG (paralague elimination during neighbouring)
            my %OGofcopy_from = %{$OG_ofcopy{$orderedOG}};
            #asses presence of genome for print
            my $asses;
            my @fromOGofcopys = sort keys %OGofcopy_from;
            for my $gi (@fromOGofcopys) {
                if ($gi =~ $acces) {
                    $asses = $gi;
                    last;
                }
            }
            #print strand and coordinate for the present genome
            if ($asses) {
                $asses =~ s/ACC\d+\_//;
                my $basegi = get_aGI($asses);
                my $order = get_order($basegi);
                $basegi =~ s/\S+\ \S+\@//;
                $basegi =~s/\-\d+//;
                print {$cooout} $basegi . ":" .  $strand_for{$asses} . ":" . $seen_gi{$asses}{min} . "-" . $seen_gi{$asses}{max} . "\t";
                print {$gbkout} $basegi . ":" .  $strand_for{$asses} . ":" . $order . "\t";
            }
            else {
                print {$cooout} "XXXXXXX" . ":" . "0" . "-" . "c" . "-" . "c" . "\t";
                print {$gbkout} "XXXXXXX" . ":" . "0" . "\t";
            }
        }
        print {$cooout} "\n";
        print {$gbkout} "\n";
    }

    return;
}

#print summary file
sub print_sum {
    my %syntenic_block = %{$_[0]};
    my $id = $_[1];
    my %event_record = %{$_[2]};
    my %OG_ofcopy = %{$_[3]};
    my %function_record = %{$_[4]};

    #take syntenic block
    my @blocks = @{$syntenic_block{$id}};
    my $size = scalar @blocks;

    #print summary informations on @blocks
    print {$sum} ">" . " " . $id . "\n";
    print {$sum} "\n";
    print {$sum} "Syntenic block size" . " : " . " " . $size . "\n";
    print {$sum} "Number of inversion event" . " : " . " " . $event_record{$id}{inversion} . "\n";
    print {$sum} "Number of missing event" . " : " . " " . $event_record{$id}{missing} . "\n";
    print {$sum} "Number of out-of-order event" . " : " . " " . $event_record{$id}{unfound} . "\n";
    print {$sum} "\n";
    #print function
    print {$sum} "Gene :" . "\n";
    for my $OG (@blocks) {
        print {$sum} $OG . ":";
        #take gi of OG (after neighbouring)
        my %OGofcopy_from = %{$OG_ofcopy{$OG}};
        my @fromOGofcopys = keys %OGofcopy_from;
        for my $gi (@fromOGofcopys) {
            my $basegi = get_aGI($gi);
            if ($function_record{$basegi}) {
                print {$sum} $function_record{$basegi} . ";";
            }
            else {
                print {$sum} 'unknown' . ";";
            }
        }
        print {$sum} "\n";
    }

    return;
}

#print fasta prot file
sub print_fasta {
    my %syntenic_block = %{$_[0]};
    my %OG_ofcopy = %{$_[1]};
    my $id = $_[2];
    my %assembly_id = %{$_[3]};
    my %seen_gi = %{$_[4]};

    my @blocks = @{$syntenic_block{$id}};
    for my $OG (@blocks) {
        #open outfile, reprint OG after neighbouring
        my $writeOG = $OG;
        $writeOG =~ s/\S+\///;
        $writeOG =~ s/\-st\.fasta//;
        my $name = "$id-$writeOG-st.faa";
        open my $printOG, '>', $name;
        #take gi of OG (after neighbouring)
        my %OGofcopy_from = %{$OG_ofcopy{$OG}};
        my @fromOGofcopys = keys %OGofcopy_from;
        #take base gi
        for my $gi (@fromOGofcopys) {
            $gi =~ s/ACC\d+\_//;
            my $basegi = get_aGI($gi);
            my $assembly = $assembly_id{$basegi};
            if ($basegi =~ '@') {
                my @chunks = split /\@/, $basegi;
                $basegi = $chunks[1];
                $assembly = $assembly_id{$gi};
            }
            #take prot sequence in OG file, take next line after $basegi as prot sequence
            my $prot = $seen_gi{$gi}{seq};
            print {$printOG} ">" . $assembly . "|" . $basegi . "\n";
            #print {$printOG} $prot . "\n";
            my $len = length $prot;
            if ($len <= 60) {
                print{$printOG} $prot . "\n";
            }
            else {
                for (my $i = 0; $i <= $len; $i += 60) {
                    my $seg =  substr($prot, $i, 60);
                    print {$printOG} $seg . "\n";
                }
            }
        }
    }

    return;
}

#print OG prot file
sub print_OG {
    my %OGlist_of = %{$_[0]};
    my %OG_ofcopy = %{$_[1]};
    my %assembly_id = %{$_[2]};
    my %seen_gi = %{$_[3]};

    my @blocks = keys %OGlist_of;
    for my $OG (@blocks) {
        #open outfile, reprint OG after neighbouring
        my $writeOG = $OG;
        $writeOG =~ s/\S+\///;
        $writeOG =~ s/\-st\.fasta//;
        my $name = "$writeOG-col.faa";
        open my $printOG, '>', $name;
        #take gi of OG (after neighbouring)
        my %OGofcopy_from = %{$OG_ofcopy{$OG}};
        my @fromOGofcopys = keys %OGofcopy_from;
        #take base gi
        for my $gi (@fromOGofcopys) {
            my $basegi = get_aGI($gi);
            my $assembly = $assembly_id{$basegi};
            #take prot sequence in OG file, take next line after $basegi as prot sequence
            my $prot = $seen_gi{$gi}{seq};
            print {$printOG} ">" . $assembly . "|" . $basegi . "\n";
            my $len = length $prot;
            if ($len <= 60) {
                print{$printOG} $prot . "\n";
            }
            else {
                for (my $i = 0; $i <= $len; $i += 60) {
                    my $seg =  substr($prot, $i, 60);
                    print {$printOG} $seg . "\n";
                }
            }
        }
    }

    return;
}

#extract accesion number
sub get_acces {
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $acces = $chunks[4];
    $acces =~ s/acc://;
    return $acces;
}
#extract neighbour gi
sub get_neighbour {
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $couple = $chunks[7];
    my @outGIs = split /\:/, $couple;
    my $preGI = $outGIs[2];
    $preGI =~ s/\(\S+\)\,GI//g;
    my $postGI = $outGIs[3];
    $postGI =~ s/\(\S+\)//g;
    return ($preGI, $postGI);
}
#extract central gi
sub get_central {
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $GI = $chunks[0];
    $GI =~ s/\>GI://;
    return $GI;
}
#extract coordinate
sub get_coordinate {
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $position = $chunks[6];
    $position =~ s/coo://;
    my @poss = split /\-/, $position;
    my $min = $poss[0];
    my $max = $poss[1];
    return ($min, $max);
}
#get strand
sub get_strand {
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $strand = $chunks[1];
    $strand =~ s/strand://g;
    return $strand;
}
#get assembly
sub get_assembly {
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $gca = $chunks[2];
    $gca =~ s/assembly://g;
    return $gca;
}
#get organism
sub get_org {
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $org = $chunks[3];
    $org =~ s/org://g;
    return $org;
}
#get product
sub get_product{
    my $line = shift;
    my @chunks = split /\|/, $line;
    my $product = $chunks[5];
    $product =~ s/product://g;
    return $product;
}
#get order
sub get_order {
    my $line = shift;
    #my @chunks = split /\|/, $line;
    #my $GI = $chunks[0];
    my $GI = $line;
    $GI =~ s/\>GI://;
    $GI =~ s/\/\S+//;
    my @GIs = split /\-/, $GI;
    my $order = $GIs[1];
    return $order;
}
#get scaffolds/chr
sub get_chr {
    my $line = shift;
    my $GI = $line;
    $GI =~ s/\>GI://;
    $GI =~ s/\/\S+//;
    my @GIs = split /\-/, $GI;
    my $chr = $GIs[2];
    return $chr;
}
#get acces from aGI
sub get_aGI {
    my $line = shift;
    my @chunks = split /\//, $line;
    my $gi = $chunks[0];
    return $gi;
}
sub get_aAcc{
    my $line = shift;
    my @chunks = split /\//, $line;
    my $acc = $chunks[1];
    return $acc;
}



__END__

=head1 NAME

MESYRES.pl - Recover syntenic and collinear regions among an OG collection

=head1 VERSION

version 0.1

=head1 USAGE

    MESYRES.pl <indir> [optional arguments]

=head1 REQUIRED ARGUMENTS

=over

=item <indir>

Path to input directory.

=for Euclid:
    indir.type: string

=back

=head1 OPTIONS

=over

=item --negate-path=<string>

List of assemblies to ignore in the analyses [default: string.default].

=for Euclid:
    string.type: string
    string.default: 'no'

=item --filtering

Filter input OG files and check the possibility of neighbours [default: no].

=item --pairwise-similarity=<number>

Minimum percentage of neighbours between two input OG files [default:
number.default].

=for Euclid:
    number.type: string
    number.default: '60'

=item --genomepath=<string>

Path to genomes (in format acc.gbk) [default: string.default].

=for Euclid:
    string.type: string
    string.default: 'genomes'

=item --minsize-neighbour=<size>

Minimum size for a neighbour stretch to be considered in colinear blocks
[default: size.default].

=for Euclid:
    size.type: string
    size.default: '2'

=item --colinear-grouping=<grouping>

Prevent creating colinear blocks on multi-chromosomes: either separate or merge
[default: grouping.default].

=for Euclid:
    grouping.type: string
    grouping.default: 'separate'

=item --coordinate-map=<coordinate>

Name of mapping coordinate group [default: coordinate.default].

=for Euclid:
    coordinate.type: string
    coordinate.default: 'colinear-group_coordinate.map'

=item --gbknumber-map=<gbk>

Name of mapping GBK number group [default: gbk.default].

=for Euclid:
    gbk.type: string
    gbk.default: 'colinear-group_gbk.map'

=item --mingenome-colinear=<mincol>

Minimum percentage of genome presence in a colinear group to be considered for
creating colinear blocks [default: mincol.default].

=for Euclid:
    mincol.type: string
    mincol.default: '60'

=item --iteration-cycle=<iteration>

Number of cycle iterations for ordering groups. Must be less than the number of
assemblies [default: iteration.default].

=for Euclid:
    iteration.type: string
    iteration.default: '0'

=item --maxgenome-block=<maxblock>

Maximum number of missing genomes in a colinear block to be retained
[default: maxblock.default].

=for Euclid:
    maxblock.type: string
    maxblock.default: '1'

=item --inversion=<inversion>

Maximum number of inversions for each OG [default: inversion.default].

=for Euclid:
    inversion.type: string
    inversion.default: '0'

=item --minsize-block=<minblock>

Minimum size for a block to be considered [default: minblock.default].

=for Euclid:
    minblock.type: string
    minblock.default: '3'

=item --summary=<summary>

Name of syntenic block summary outfile [default: summary.default].

=for Euclid:
    summary.type: string
    summary.default: 'syntenic-block.sum'

=item --print-fasta

Print protein file in FASTA format [default: no].

=item --forty2

Pre-run of 42 [default: no].

=item --print-col

Print FASTA files involved in colinear groups for 42 [default: no].

=back

=head1 AUTHOR

Luc CORNET <luc.cornet@uliege.be>

=head1 CONTRIBUTOR

Denis BAURAIN <denis.baurain@uliege.be>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Liege / Unit of Eukaryotic
Phylogenomics / Luc CORNET and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
