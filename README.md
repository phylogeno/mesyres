# MESYRES  

**MESYRES** is a program developed to recover syntenic and collinear regions among a collection of orthologous groups (OGs), such as those produced with **OrthoFinder**, enriched with sequence fragments from metagenomic contigs, using **FortyTwo**.

The MESYRES pipeline is composed of two scripts:  

- `gbk-reader.pl`: the pre-processor creates FASTA files from GenBank GBFF files and reformat the definition lines for the orthogroup inference program.
- `MESYRES.pl`: the main script runs a six-step greedy algorithm to determine Syntenic Orthologous Regions (SORs).

## Install  

### 1. Perl packages  

`MESYRES.pl` and `gbk-reader.pl` are simple Perl executables but they require de presence of some prerequisites in the system. Most missing Perl packages can be installed all at once using `cpanm` on `Bio::MUST::Core`.

One way is to setup a `perlbrew` environment. If you already have such an environmnent, you have everything you need. Otherwise, proceed following the instructions below (these are taken from FortyTwo's manual; see below).

      # install development tools
      $ sudo apt-get update
      $ sudo apt-get install build-essential
 
      # download the perlbrew installer...
      $ wget -O - http://install.perlbrew.pl | bash
 
      # initialize perlbrew
      $ source ~/perl5/perlbrew/etc/bashrc
      $ perlbrew init
 
      # search for a recent stable version of the perl interpreter
      $ perlbrew available
      # install the last even (= stable) version (e.g., 5.24.x, 5.26.x, 5.28.x)
      # (this will take a while)
      $ perlbrew install perl-5.26.2
      # install cpanm (for Perl dependencies)
      $ perlbrew install-cpanm
 
      # enable the just-installed version
      $ perlbrew list
      $ perlbrew switch perl-5.26.2
 
      # make perlbrew always available
      # if using bash (be sure to use double >> to append)
      $ echo "source ~/perl5/perlbrew/etc/bashrc" >> ~/.bashrc
      # if using zsh  (only the destination file changes)
      $ echo "source ~/perl5/perlbrew/etc/bashrc" >> ~/.zshrc

      # install Bio::MUST::Core and associated dependencies
      $ cpanm Bio::MUST::Core

### 2. OrthoFinder

Follow the guidelines provided in the link below:\
https://github.com/davidemms/OrthoFinder

### 3. MESYRES

Clone this repository to download `MESYRES.pl` and `gbk-reader.pl`.

## Run

1. Create FASTA files from GenBank GBFF files

      ```
      $ ./gbk-reader.pl ```*```.gbk --create-fasta=yes
      ```

2. Run OrthoFinder on the generated FASTA files (see OrthoFinder manual)
3. Reformat OG deflines

      ```
      $ ./gbk-reader.pl ```*```.gbk --create-defline=yes --ogpath=PATH-TO-OGs-FASTA-DIR/
      ```
      
4. Run `MESYRES.pl` on reformated OGs

      ```
      $ ./MESYRES.pl PATH-TO-OGS-REFOMATED/ --filtering --pairwise-similarity=100 --maxgenome-block=0 --inversion=0 2> log
      ```
      
5. Run Forty-Two on OGs corresponding to SORs (see configuration files in this repository)\
https://metacpan.org/release/Bio-MUST-Apps-FortyTwo

6. Run `MESYRES.pl` on these 42-enriched OGs

      ```
      $ MESYRES.pl forty/ali/ --filtering --pairwise-similarity=100 --maxgenome-block=0 --inversion=0 --forty2 --mingenome-colinear=40 --print-fasta  2> log  
      ```

## Copyright and License

This software is copyright (c) 2017 by University of Liege / Unit of Eukaryotic Phylogenomics / Luc CORNET and Denis BAURAIN.

This is free software; you can redistribute it and/or modify it under the same terms as the Perl 5 programming language system itself.

